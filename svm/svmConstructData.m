function [svm_cases, svm_labels] = svmConstructData (cases, labels, dataType, seqNum, stage, timeBefore)
%====================================================================================
% cases:        Cases that needs to construct, different dataType has different 
%               number of kinds.
% labels:       Labels of the cases.
% dataType:     If it's one of ['LLB';'MC';'P','dataDriven']
% stage:        Whether cases of all process or approach stage should be constructed.
%               It's one of ['All', 'Approach']
%====================================================================================
% svm_cases:    Constructed cases. 
% svm_labels:   Constructed labels.
%====================================================================================

    casesNum = size(cases, 1);
    labelNum = size(labels, 1);
    svm_cases = [];

    assert (casesNum == labelNum || labelNum == 1, ...
    'In svm construct data stage, number of labels should be 1 or equal to number of cases');

    assert (   strncmp(dataType, 'LLB', 3)    ...
            || strncmp(dataType, 'MC',2)      ...
            || strncmp(dataType, 'P', 1) ,    ...
    'We only develop for these three types of data');

    assert (   strncmp(stage, 'All', 3)       ...
            || strncmp(stage, 'Approach', 8), ...
    'We only develop for these two stages.');


    if (labelNum == 1)
        svm_labels = ones(casesNum, 1) .* labels;
    else 
        svm_labels = labels;
    end

    for i = 1:casesNum
        if (length(who('timeBefore')) > 0)
            [fx, fy, fz, mx, my, mz] = labelSeqBefore(cases(i, :), dataType, seqNum, stage, timeBefore);
        else
            [fx, fy, fz, mx, my, mz] = labelSeqBefore(cases(i, :), dataType, seqNum, stage);
        end
        svm_cases = [svm_cases; fx, fy, fz, mx, my, mz];
    end

end
