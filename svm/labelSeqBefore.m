function [fx, fy, fz, mx, my, mz, caseName] = labelSeqBefore(caseSample, dataType, seqNum, stage, timeBefore)

    caseName = caseSample(1,8).data;

    switch (dataType)
    case {'LLB'}    
        dataCell = 1; timeCell = 13; dataTypeSize =  8; caseSize =  8 ^ seqNum;
    case {'MC'}     
        dataCell = 2; timeCell =  8; dataTypeSize = 10; caseSize = 10 ^ seqNum;
    case {'P'}      
        dataCell = 8; timeCell =  5; dataTypeSize = 10; caseSize = 10 ^ seqNum;
    endswitch
    
    if (length(who('timeBefore')) == 0)
	    switch (stage)
	    case {'Approach'}
	        timeBefore = caseSample(1,7).data(2);
	    case {'All'}
	        timeBefore = 9999;
        endswitch;
    end
    
    for i = 1:6
        labelSeq = zeros (1, caseSize);
        labelSize = sum(caseSample(1, i).data(:,timeCell) < timeBefore, 1);
        for j = 1:(labelSize - seqNum + 1)
            labelSeqPos = 0;
            for k = 1:seqNum
                labelSeqPos = labelSeqPos * dataTypeSize + caseSample(1,i).data(j + k - 1, dataCell) - 1;
            end
            ++ labelSeqPos;
            ++ labelSeq (labelSeqPos) ;
        end
        if (i == 1) fx = labelSeq; end
        if (i == 2) fy = labelSeq; end
        if (i == 3) fz = labelSeq; end
        if (i == 4) mx = labelSeq; end
        if (i == 5) my = labelSeq; end
        if (i == 6) mz = labelSeq; end
   end
end
