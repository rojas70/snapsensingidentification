function acc = svmPredict (rootPath, dataType, seqNum, stage, maxCases)
%============================================================
% rootPath: Root path that the cases are stored.
% dataType: Which kind of data you want? ['LLB', 'MC', 'P']
% seqNum: Number of labels for a seq.
% stage: ['Approach', 'All'] ?
%============================================================
% acc: Accuracy.
%============================================================
    global svm_SxF;
    global svm_dataDriven_SxF;
    global svm_Fsub;

    global svm_P_approach_process;
    
    switch (dataType)
    case {'LLB'}
	    successCases = loadLLBData (rootPath, 'success');
	    failureCases = loadLLBData (rootPath, 'FC');
	    experimentCases = loadLLBData (rootPath, 'exp');
	    prelimCases = loadLLBData (rootPath, 'prelim');
    case {'MC'}
	    successCases = loadMCData (rootPath, 'success');
	    failureCases = loadMCData (rootPath, 'FC');
	    experimentCases = loadMCData (rootPath, 'exp');
	    prelimCases = loadMCData (rootPath, 'prelim');
    case {'P'}
	    successCases = loadPData (rootPath, 'success');
	    failureCases = loadPData (rootPath, 'FC');
	    experimentCases = loadPData (rootPath, 'exp');
	    prelimCases = loadPData (rootPath, 'prelim');
    case{'dataDriven'}
        assert('No implemented');
	endswitch

    svm_cases  = [];
    svm_results = [];
    test_cases  = [];
    test_results = [];
    svm_cases_name = [];
    test_cases_name = [];
   
    for i = 1:size(successCases, 1)
        svm_cases_name = [svm_cases_name; successCases(i,8).data];
    end
    for i = 1:size(prelimCases, 1)
        svm_cases_name = [svm_cases_name; prelimCases(i,8).data];
    end
    for i = 1:size(failureCases, 1)
        svm_cases_name = [svm_cases_name; failureCases(i,8).data];
    end
    for i = 1:size(experimentCases, 1)
        svm_cases_name = [svm_cases_name; experimentCases(i,8).data];
    end

    if (svm_dataDriven_SxF == 1 && strncmp(dataType, 'dataDriven', 10))
    %========================================================
    %   1: Success cases.
    %  -1: Failure cases.
        
        



    end

    if (svm_SxF == 1 && !strncmp(dataType, 'dataDriven', 10))
    %========================================================
    %  1 : Suceess cases.
    % -1 : Failure cases.
    
        successNum = size(successCases, 1) + size(prelimCases, 1);
        failureNum = size(failureCases, 1) + size(experimentCases, 1);

        portion = successNum / (failureNum + successNum);

        [tmp_cases, tmp_results] = svmConstructData (successCases, 1, dataType, seqNum, stage);
        svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

        [tmp_cases, tmp_results] = svmConstructData (prelimCases, 1, dataType, seqNum, stage);
        svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

        [tmp_cases, tmp_results] = svmConstructData (failureCases, -1, dataType, seqNum, stage);
        svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

        [tmp_cases, tmp_results] = svmConstructData (experimentCases, -1, dataType, seqNum, stage);
        svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];
        
        index_success = randperm (successNum);
        index_failure = randperm (failureNum);

        successTrainNum = floor(maxCases * portion);
        failureTrainNum = floor(maxCases * (1 - portion));

        index_train = [ index_success(1 : ceil(successTrainNum/2)), ...
                        index_failure(1 : ceil(failureTrainNum/2)) .+ successNum];
        index_test = [  index_success((ceil(successTrainNum/2)+1) : successTrainNum), ...
                        index_failure((ceil(failureTrainNum/2)+1) : failureTrainNum) .+ successNum];
        index_test_succ = [index_success((ceil(successTrainNum/2)+1) : successTrainNum)];
        index_test_fail = [index_failure((ceil(failureTrainNum/2)+1) : failureTrainNum) .+ successNum];

        SVMstruct = svmtrain(svm_results(index_train, :), svm_cases(index_train, :), '-q');
        predict = svmpredict(svm_results(index_test, :), svm_cases(index_test, :), SVMstruct);

        predictSuc = svmpredict(svm_results(index_test_succ, :), ...
                                svm_cases(index_test_succ, :), SVMstruct);

        predictFai = svmpredict(svm_results(index_test_fail, :), ...
                                svm_cases(index_test_fail, :), SVMstruct);

        totalAcc = sum (predict == svm_results(index_test, :)) / size(predict, 1);
        succAcc  = sum (predictSuc == svm_results(index_test_succ, :)) / size(predictSuc, 1);
        failAcc  = sum (predictFai == svm_results(index_test_fail, :)) / size(predictFai, 1);

        acc = [ successTrainNum + failureTrainNum, totalAcc, successTrainNum, succAcc, failureTrainNum, failAcc];
        acc

        if (svm_P_approach_process == 1)
            for i = 1:400
                [tmp_cases, tmp_results] = svmConstructData (successCases(1,:), 1, dataType, seqNum, stage, 0.01 * i);
                test_cases = [test_cases; tmp_cases]; test_results = [test_results; tmp_results];
            end
            predict = svmpredict(test_results, test_cases, SVMstruct);
        end

    end
    %========================================================

    if (svm_Fsub == 1&& !strncmp(dataType, 'dataDriven', 10))
    %========================================================
    % 0 : no diviation
    % 1 : diviation of +x
    % 2 : diviation of +y
    % 3 : diviation of -y
    % 4 : diviation of +Yall
    % 5 : diviation of -Yall
    % 6 : diviation of +x+y
    % 7 : diviation of +x-y
    % 8 : diviation of +x+Yall
    % 9 : diviation of +x-Yall
    %10 : diviation of +y+Yall
    %11 : diviation of -y+Yall
    %12 : diviation of +x+y+Yall
    %13 : Otherwise.

        cases = [successCases; failureCases; experimentCases; prelimCases]; 
        casesNum = size (cases, 1); 
        caseNames = [];
        for i = 1:casesNum
            caseName = cases(i, 8).data;
            if (strncmp(caseName, 'success', 7))
                [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 0, dataType, seqNum, stage);
                svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];
                caseNames = [caseNames; caseName];
            end

            if (strncmp(caseName, 'FC', 2))
                caseNames = [caseNames; caseName];
                caseAnalysis = substr(caseName, 6, length(caseName) - 5);
                
                if (strncmp(caseAnalysis, '+x', 2)) % +x
                    [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 1, dataType, seqNum, stage);
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                elseif (strncmp(caseAnalysis, '+y', 2)) % +y
                    [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 2, dataType, seqNum, stage);
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                elseif (strncmp(caseAnalysis, '-y', 2)) % -y
                    [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 3, dataType, seqNum, stage);
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                elseif (strncmp(caseAnalysis, '_xYall', 6)) % Yall axis only
                    if (strfind(caseAnalysis, '+'))
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 4, dataType, seqNum, stage);
                    else
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 5, dataType, seqNum, stage);
                    end
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                elseif (strncmp(caseAnalysis, '_XY', 3)) % x and y axis
                    if (strfind(caseAnalysis, '+y'))
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 6, dataType, seqNum, stage);
                    else
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 7, dataType, seqNum, stage);
                    end
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                elseif (strncmp(caseAnalysis, '_XxYall', 7))
                    if (strfind(caseAnalysis, '+0'))
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 8, dataType, seqNum, stage);
                    else
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 9, dataType, seqNum, stage);
                    end
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                elseif (strncmp(caseAnalysis, '_YxYall', 7))
                    if (strfind(caseAnalysis, '+y'))
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 10, dataType, seqNum, stage);
                    else
                        [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 11, dataType, seqNum, stage);
                    end
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                elseif (strncmp(caseAnalysis, '_xyYall', 7))
                    [tmp_cases, tmp_results] = svmConstructData(cases(i, :), 12, dataType, seqNum, stage);
                    svm_cases = [svm_cases; tmp_cases]; svm_results = [svm_results; tmp_results];

                end

            end
        end

        SVMstruct = svmtrain(svm_results, svm_cases, '-q');
        predict = svmpredict (svm_results, svm_cases, SVMstruct);
%        for i = 1:length(predict) 
%            if (strncmp(caseNames(i, :), 'FC00', 4))
%                predict(i)
%            end
%        end
%        sortrows( caseNames(predict == svm_results, :))
%        sortrows( caseNames(predict != svm_results, :))
%        caseNames(predict != svm_results, :)
%        predict(predict != svm_results, :)





    end
    %======================================================== 
end
