function [cases] = buildMCData (rootPath, casesBeginWith) 
%=======================================================
% Load data from a txt. And 
    cases = [];
    anaCases = ls (rootPath);
    caseNum = 0;
    for i = 1:size(anaCases)
        casePath = anaCases(i, :);
        if (strncmp (casePath, casesBeginWith, size(casesBeginWith, 2)))          
            casePath
            caseNum ++;
            
            paths = [   strcat(rootPath, '/', casePath, '/', 'Composites', '/', 'Composites_Fx.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Composites', '/', 'Composites_Fy.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Composites', '/', 'Composites_Fz.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Composites', '/', 'Composites_Mx.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Composites', '/', 'Composites_My.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Composites', '/', 'Composites_Mz.txt')   ];

            for j = 1:6
                fileOpen = fopen (paths(j, :), 'r');
                data = [];
                forOnceFlag = 0;
                dataLine = zeros(1, 12);
                %==========================================================
                % Bugs here. Can't handle the last dataLine.
                % Think it more clear.
                while (~feof (fileOpen))
                    line = fgetl(fileOpen);
                    if (strncmp(line , 'Iteration', 9)) 
                        iteration = strsplit (line, ':')(1, 2);
                        iteration = cell2mat(iteration);

                        if (str2num (iteration) == 1 && forOnceFlag == 1)
                            break;
                        elseif (str2num (iteration) == 1)
                            forOnceFlag = 1;
                        end

                        if ( sum(dataLine) != 0 )
                            data = [data; dataLine];
                        end

                        dataLine = zeros(1, 12);
                        dataLine(1) = str2num(iteration);

                    elseif (strncmp(line, 'Label 1', 7))
                        label1 = strsplit (line, ':')(1,2);
                        label1 = cell2mat(label1);
                        label1 = label1(~isspace(label1));
                        dataLine(6) = primitiveLabel(label1);

                    elseif (strncmp(line, 'Label 2', 7))
                        label2 = strsplit (line, ':')(1,2);
                        label2 = cell2mat(label2);
                        label2 = label2(~isspace(label2));
                        dataLine(7) = primitiveLabel(label2);

                    elseif (strncmp(line, 'Label', 5))
                        label = strsplit (line, ':')(1,2);
                        label = cell2mat(label);
                        label = label(~isspace(label));
                        dataLine(2) = MCLabel(label);
                        
                    elseif (strncmp(line, 'Average Val', 11))
                        averVal = strsplit (line, ':')(1,2);
                        averVal = cell2mat(averVal);
                        dataLine(3) = str2num(averVal);

                    elseif (strncmp(line, 'RMS Val', 7))
                        RMSVal = strsplit (line, ':')(1,2);
                        RMSVal = cell2mat(RMSVal);
                        RMSVal = RMSVal(~isspace(RMSVal));
                        dataLine(4) = str2num(RMSVal);

                    elseif (strncmp(line, 'Amplitude Val', 13))
                        AmpVal = strsplit (line, ':')(1,2);
                        AmpVal = cell2mat(AmpVal);
                        AmpVal = AmpVal(~isspace(AmpVal));
                        dataLine(5) = str2num(AmpVal);

                    elseif (strncmp(line, 't1Start', 7))
                        t1start = strsplit (line, ':')(1,2);
                        t1start = cell2mat(t1start);
                        t1start = t1start(~isspace(t1start));
                        dataLine(8) = str2num(t1start);

                    elseif (strncmp(line, 't1End', 5))
                        t1end = strsplit (line, ':')(1,2);
                        t1end = cell2mat(t1end);
                        t1end = t1end(~isspace(t1end));
                        dataLine(9) = str2num(t1end);

                    elseif (strncmp(line, 't2Start', 7))
                        t2start = strsplit (line, ':')(1,2);
                        t2start = cell2mat(t2start);
                        t2start = t2start(~isspace(t2start));
                        dataLine(10) = str2num(t2start);

                    elseif (strncmp(line, 't2End', 5))
                        t2end = strsplit (line, ':')(1,2);
                        t2end = cell2mat(t2end);
                        t2end = t2end(~isspace(t2end));
                        dataLine(11) = str2num(t2end);

                    elseif (strncmp(line, 'tAvgIndex', 9))
                        tavgIndex = strsplit (line, ':')(1,2);
                        tavgIndex = cell2mat(tavgIndex);
                        tavgIndex = tavgIndex(~isspace(tavgIndex));
                        dataLine(12) = str2num(tavgIndex);
                    end
                end
                data = [data; dataLine];
                save(strrep(paths(j,:), 'txt', 'mat'), 'data') 
                
            end
        end
    end
end
