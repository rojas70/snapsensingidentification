function [cases] = buildPrimitiveData (rootPath, casesBeginWith) 
%=======================================================
% Load data from a txt. And 
    cases = [];
    anaCases = ls (rootPath);
    caseNum = 0;
    for i = 1:size(anaCases)
        casePath = anaCases(i, :);
        if (strncmp (casePath, casesBeginWith, size(casesBeginWith, 2)))          
            casePath
            caseNum ++;
            paths = [   strcat(rootPath, '/', casePath, '/', 'Segments', '/', 'Segement_Fx.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Segments', '/', 'Segement_Fy.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Segments', '/', 'Segement_Fz.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Segments', '/', 'Segement_Mx.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Segments', '/', 'Segement_My.txt'); ...
                        strcat(rootPath, '/', casePath, '/', 'Segments', '/', 'Segement_Mz.txt')   ];

            for j = 1:6
                fileOpen = fopen (paths(j, :), 'r');
                data = [];
                forOnceFlag = 0;
                dataLine = zeros(1, 8);
                while (~feof (fileOpen))
                    line = fgetl(fileOpen);
                    if (strncmp(line , 'Iteration', 9)) 
                        iteration = strsplit (line, ':')(1, 2);
                        iteration = cell2mat(iteration);
                       
                        if (str2num (iteration) == 1 && forOnceFlag == 1)
                            break;
                        elseif (str2num (iteration) == 1)
                            forOnceFlag = 1;
                        end

                        if ( sum(dataLine) != 0 )
                            data = [data; dataLine];
                        end

                        dataLine = zeros(1, 8);
                        dataLine(1) = str2num(iteration);

                    elseif (strncmp(line, 'Grad Label', 10))
                        label = strsplit (line, ':')(1,2);
                        label = cell2mat(label);
                        label = label(~isspace(label));
                        dataLine(8) = primitiveLabel(label);

                    elseif (strncmp(line, 'Average', 7))
                        averVal = strsplit (line, ':')(1,2);
                        averVal = cell2mat(averVal);
                        dataLine(2) = str2num(averVal);

                    elseif (strncmp(line, 'Max Val', 7))
                        MaxVal = strsplit (line, ':')(1,2);
                        MaxVal = cell2mat(MaxVal);
                        MaxVal = MaxVal(~isspace(MaxVal));
                        dataLine(3) = str2num(MaxVal);

                    elseif (strncmp(line, 'Min Val', 7))
                        MinVal = strsplit (line, ':')(1,2);
                        MinVal = cell2mat(MinVal);
                        MinVal = MinVal(~isspace(MinVal));
                        dataLine(4) = str2num(MinVal);

                    elseif (strncmp(line, 'Start', 5))
                        start = strsplit (line, ':')(1,2);
                        start = cell2mat(start);
                        start = start(~isspace(start));
                        dataLine(5) = str2num(start);

                    elseif (strncmp(line, 'Finish', 6))
                        finish = strsplit (line, ':')(1,2);
                        finish = cell2mat(finish);
                        finish = finish(~isspace(finish));
                        dataLine(6) = str2num(finish);

                    elseif (strncmp(line, 'Gradient', 8))
                        gradient = strsplit (line, ':')(1,2);
                        gradient = cell2mat(gradient);
                        gradient = gradient(~isspace(gradient));
                        dataLine(7) = str2num(gradient);

                    end
                end
                data = [data;dataLine];
                if (size(data, 1) == 0)
                    printf(paths(j,:));
                end
                save(strrep(paths(j,:), 'txt', 'mat'), 'data'); 
                
            end
        end
    end
end
