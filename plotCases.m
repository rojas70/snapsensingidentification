
function plotCases (rootPath)
    global success;
    global prelim;
    global failureX;
    global failureY;
    global failureRo;
    global failureXY;
    global failureXRo;
    global failureYRo;
    global failureXYRo;
    success     = 0;
    prelim      = 1;
    failureX    = 1;
    failureY    = 0;
    failureRo   = 0;
    failureXY   = 0;
    failureXRo  = 0;
    failureYRo  = 0;
    failureXYRo = 0;

    addpath ('./svm');
    addpath ('./load');
    
    axis = ['fx'; 'fy'; 'fz'; 'mx'; 'my'; 'mz'];
    cases = ls (rootPath);
    j = 0;

    for i = 1:size(cases, 1)
        if ( ifPlot(cases(i,:)) )
            ++ j;
        end
    end

    plotsize = ceil(sqrt(j));
    k = 1;

    for i = 1:6
        h = figure(i);
        set (h, 'name', axis(i,:)); 
        k = 1;
        for j = 1:size(cases, 1)
            if (!ifPlot(cases(j, :)))
                continue;
            end
            forceSig = load (strcat (rootPath, '/', cases(j,:), '/', 'Torques.dat'));
            state = load (strcat (rootPath, '/', cases(j,:), '/', 'State.dat'));
            approachSig = forceSig (forceSig(:,1) < state(2), :);
            subplot(plotsize, plotsize, k);
            plot(approachSig(:, 1), approachSig(:, i+1));
            title(cases(j, :));
            k ++;
        end
    end
end

function plotFlag = ifPlot (caseName)

    global failureX;
    global failureY;
    global failureRo;
    global failureXY;
    global failureXRo;
    global failureYRo;
    global failureXYRo;
    global success;
    global prelim;

    if (strncmp(caseName, 'FC', 2))
        caseAnalysis = substr(caseName, 6, length(caseName) - 5);
        if (strncmp (caseAnalysis, '+x', 2) && failureX == 1)
            plotFlag = 1;
        elseif (strncmp (caseAnalysis, '+y', 2) && failureY == 1)
            plotFlag = 1;
        elseif (strncmp (caseAnalysis, '-y', 2) && failureY == 1)
            plotFlag = 1;
        elseif (strncmp (caseAnalysis, '_xYall', 6) && failureRo == 1)
            plotFlag = 1;
        elseif (strncmp (caseAnalysis, '_XY', 3) && failureXY == 1)
            plotFlag = 1;
        elseif (strncmp (caseAnalysis, '_XxYall', 7) && failureXRo == 1)
            plotFlag = 1;
        elseif (strncmp (caseAnalysis, '_YxYall', 7) && failureYRo == 1)
            plotFlag = 1;
        elseif (strncmp (caseAnalysis, '_xyYall', 7) && failureXYRo == 1)
            plotFlag = 1;
        else
            plotFlag = 0;
        end
    elseif (strncmp(caseName, 'success', 7) && success == 1)
        plotFlag = 1;
    elseif (strncmp(caseName, 'prelim', 6) && prelim == 1)
        plotFlag = 1;
    else
        plotFlag = 0;
    end
end
